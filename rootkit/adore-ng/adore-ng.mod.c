#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x2173f702, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x237db4e1, __VMLINUX_SYMBOL_STR(skb_dequeue) },
	{ 0x82a6cf0c, __VMLINUX_SYMBOL_STR(pid_vnr) },
	{ 0x843249b2, __VMLINUX_SYMBOL_STR(skb_recv_datagram) },
	{ 0x5a921311, __VMLINUX_SYMBOL_STR(strncmp) },
	{ 0x3a0e033, __VMLINUX_SYMBOL_STR(PDE_DATA) },
	{ 0xd253b0df, __VMLINUX_SYMBOL_STR(kobject_put) },
	{ 0xca9af4b2, __VMLINUX_SYMBOL_STR(kobject_del) },
	{ 0x6cfd6dc9, __VMLINUX_SYMBOL_STR(kobject_uevent) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0x98cf60b3, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x1e6d26a8, __VMLINUX_SYMBOL_STR(strstr) },
	{ 0x2492bd3, __VMLINUX_SYMBOL_STR(filp_close) },
	{ 0xd4107204, __VMLINUX_SYMBOL_STR(filp_open) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x997a8464, __VMLINUX_SYMBOL_STR(init_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xd1c923fa, __VMLINUX_SYMBOL_STR(d_alloc) },
	{ 0x73373d0a, __VMLINUX_SYMBOL_STR(dput) },
	{ 0x1315bfce, __VMLINUX_SYMBOL_STR(iput) },
	{ 0x3351afa2, __VMLINUX_SYMBOL_STR(d_lookup) },
	{ 0x6f20960a, __VMLINUX_SYMBOL_STR(full_name_hash) },
	{ 0x5cd885d5, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x1fdc7df2, __VMLINUX_SYMBOL_STR(_mcount) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "E9BE2BE87C68882D80C65E7");
