#define _GNU_SOURCE
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/syscall.h>

#define handle_error(msg) \
        do { perror(msg); exit(EXIT_FAILURE); } while (0)

#define LOOP_NUM 2000      
#define BUF_SIZE 100

// system_call:
// 1) sys_getdents64 (61)  
// 2) sys_read       (63)  
// 3) sys_write      (64) 
// 4) sys_openat     (56)
// 5) sys_close      (57)        

int main(int argc, char *argv[])
{
    int fd1, fd2, n, i;
    char buf1[BUF_SIZE];
    char buf2[BUF_SIZE] = "test content for read_write";

    for (i=0; i<LOOP_NUM; i++) {

        fd1 = open("/home/root/lrk_det/test_folder/", O_RDONLY | O_DIRECTORY);
        if (fd1 == -1)
            handle_error("open folder failed");
        
        n = syscall(SYS_getdents64, fd1, buf1, BUF_SIZE);
        if (n == -1)
            handle_error("sys_getdents64");

        fd2 = syscall(SYS_openat, 0, "/home/root/lrk_det/test_folder/test_file", O_RDWR, 0640);
        if (n == -1)
            handle_error("sys_openat");

        n = syscall(SYS_read, fd2, buf2, BUF_SIZE);
        if (n == -1)
            handle_error("sys_read");

        n = syscall(SYS_write, fd2, buf2, BUF_SIZE);
        if (n == -1)
            handle_error("sys_write");

        n = syscall(SYS_close, fd2);
        if (n == -1)
            handle_error("sys_close");

        close(fd1);
    }

    exit(EXIT_SUCCESS);
}



