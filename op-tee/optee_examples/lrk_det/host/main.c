#include <err.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/syscall.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <asm/ptrace.h>
#include <asm/unistd.h>
#include <linux/elf.h>
#include <linux/kernel.h>  
#include <tee_client_api.h>
#include <lrk_det_ta.h>

typedef unsigned int u32;

#define pt_regs  user_pt_regs 
#define ARM_pc   pc
#define ARM_x8   regs[8]

u32 count_syscall = 0;
u32 count_flag = 0;

static void config_pmn(u32 counter, u32 event);
static void enable_pmns(void);
static void disable_pmns(void);
static void reset_pmns(void);
static void config_ctrs(void);
int getSysCallNo(int pid, struct pt_regs *regs);
void tracePro(int pid,  TEEC_Session sess,  TEEC_Operation op);

int main(int argc, char *argv[])
{
	TEEC_Result res;
	TEEC_Context ctx;
	TEEC_Session sess;
	TEEC_Operation op;
	TEEC_UUID uuid = TA_LKR_DET_UUID;
	u32 err_origin;
	pid_t traced_process;
    int status, i;
    char bar[100];
    const char* lable="|/-\\";

    if (argc != 2) {
        printf("Wrong parameters!\n");
        return 1;
    } else {
        if (strcmp(argv[1], "set") == 0) {
            config_ctrs();

            res = TEEC_InitializeContext(NULL, &ctx);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InitializeContext failed with code 0x%x", res);

            res = TEEC_OpenSession(&ctx, &sess, &uuid, TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_Opensession failed with code 0x%x origin 0x%x", res, err_origin);

            memset(&op, 0, sizeof(op));
            op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INOUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);

            // ptrace test_syscall
            traced_process = fork(); 
            if (traced_process == 0) {
                ptrace(PTRACE_TRACEME, 0, NULL, NULL);
                execl("/usr/bin/test_syscall", "test_syscall", NULL);
            } else {
                if (ptrace(PTRACE_ATTACH, traced_process, NULL, NULL) != 0) {
                    printf("Trace process failed:%d.\n", errno);
                    return 1;
                }
                while(1) {
                    wait(&status);
                    if(WIFEXITED(status)) {
                        break;
                    }
                    tracePro(traced_process, sess, op);
                    ptrace(PTRACE_SYSCALL, traced_process, NULL, NULL);
                }       
            }
            
            // set hpc
            res = TEEC_InvokeCommand(&sess, TA_LKR_DET_CMD_SET_HPC, &op, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x", res, err_origin);

            TEEC_CloseSession(&sess);
            TEEC_FinalizeContext(&ctx);
        } else if (strcmp(argv[1], "check") == 0) {
            config_ctrs();  

            res = TEEC_InitializeContext(NULL, &ctx);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InitializeContext failed with code 0x%x", res);

            res = TEEC_OpenSession(&ctx, &sess, &uuid, TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_Opensession failed with code 0x%x origin 0x%x", res, err_origin);

            memset(&op, 0, sizeof(op));
            op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INOUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);

            // ptrace test_syscall
            traced_process = fork(); 
            if (traced_process == 0) {
                ptrace(PTRACE_TRACEME, 0, NULL, NULL);
                execl("/usr/bin/test_syscall", "test_syscall", NULL);
            } else {
                if (ptrace(PTRACE_ATTACH, traced_process, NULL, NULL) != 0) {
                    printf("Trace process failed:%d.\n", errno);
                    return 1;
                }
                while(1) {
                    wait(&status);
                    if(WIFEXITED(status)) {
                        break;
                    }
                    tracePro(traced_process, sess, op);
                    ptrace(PTRACE_SYSCALL, traced_process, NULL, NULL);
                }       
            }

            // check hpc
            res = TEEC_InvokeCommand(&sess, TA_LKR_DET_CMD_CHECK_HPC, &op, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x", res, err_origin);

            TEEC_CloseSession(&sess);
            TEEC_FinalizeContext(&ctx);
        } else {
            printf("Wrong parameters!\n");
            return 1;
        }
    }
	return 0;
}

int getSysCallNo(int pid, struct pt_regs *regs)
{
    u32 scno = 0;  
    struct iovec ioVec;
            
    ioVec.iov_base = regs;
    ioVec.iov_len = sizeof(*regs);
    if (ptrace(PTRACE_GETREGSET, pid, (void*)NT_PRSTATUS, &ioVec) < 0) {    
        perror("Can not get register values");   
        return 0;    
    }  
    scno = regs->ARM_x8; 
    return scno;    
}

// system_call:
// 1) sys_getdents64 (61)  
// 2) sys_read       (63)  
// 3) sys_write      (64) 
// 4) sys_openat     (56)
// 5) sys_close      (57)      
void tracePro(int pid, TEEC_Session sess,  TEEC_Operation op)
{
    u32 scno = 0;
    struct pt_regs regs;
    TEEC_Result res;
    u32 err_origin;
    
    disable_pmns();   
    scno = getSysCallNo(pid, &regs);

    if (count_flag == 0 && scno == 61) {
        count_flag = 1;       // start counting at the entry point of sys_getdents64 
    }

    if (count_flag) {
        count_syscall ++;
        if (count_syscall%2 == 1) {  
            enable_pmns();  
            reset_pmns();
        } else { 
            // read cnt
            op.params[0].value.a = scno;
            res = TEEC_InvokeCommand(&sess, TA_LKR_DET_CMD_READ_CNT, &op, &err_origin);
            if (res != TEEC_SUCCESS)
                errx(1, "TEEC_InvokeCommand failed with code 0x%x origin 0x%x", res, err_origin);
            if (scno == 57) {
                count_flag = 0; // stop counting at the exit point of sys_close
            }
        } 
    }
}

static void config_pmn(u32 counter, u32 event)
{
    u32 val = 0;

    switch(counter) {
        case 0:
            val = 0x40000000;  // Only count events in EL1
            val = val | event;
            asm volatile("msr PMEVTYPER0_EL0, %0" : : "r"(val));
            break;
        case 1:
            val = 0x40000000;  // Only count events in EL1
            val = val | event;
            asm volatile("msr PMEVTYPER1_EL0, %0" : : "r"(val));
            break;
        case 2:
            val = 0x40000000;  // Only count events in EL1 
            val = val | event;
            asm volatile("msr PMEVTYPER2_EL0, %0" : : "r"(val));
            break;
        case 3:
            val = 0x40000000;  // Only count events in EL1 
            val = val | event;
            asm volatile("msr PMEVTYPER3_EL0, %0" : : "r"(val));
            break;
        case 4:
            val = 0x40000000;  // Only count events in EL1 
            val = val | event;
            asm volatile("msr PMEVTYPER4_EL0, %0" : : "r"(val));
            break;
        case 5:
            val = 0x40000000;  // Only count events in EL1 
            val = val | event;
            asm volatile("msr PMEVTYPER5_EL0, %0" : : "r"(val));
            break;  
        default:
            break;
    }
}

static void enable_pmns(void)
{
    u32 val;

    asm volatile("mrs %0, PMCNTENSET_EL0" : "=r"(val)); 
    val = val | 0b111111;
    asm volatile("msr PMCNTENSET_EL0, %0" : : "r"(val));
}

static void disable_pmns(void)
{
    u32 val;

    asm volatile("mrs %0, PMCNTENCLR_EL0" : "=r"(val)); 
    val = val | 0b111111;
    asm volatile("msr PMCNTENCLR_EL0, %0" : : "r"(val));
}

static void reset_pmns(void)
{
    u32 val;

    asm volatile("mrs %0, PMCR_EL0" : "=r"(val)); 
    val = val | (1<<1);
    asm volatile("msr PMCR_EL0, %0" : : "r"(val));
}

static void config_ctrs(void)
{
    /*  HPC event:
     *  0. INSTRUCTIONS        --> 0x08 --> INST_RETIRED
     *  1. CACHE_REFERENCES    --> 0x04 --> L1D_CACHE
     *  2. CACHE_MISSES        --> 0x03 --> L1D_CACHE_REFILL
     *  3. BRANCH_INSTRUCTIONS --> 0x0C --> PC_WRITE_RETIRED
     *  4. BRANCH_MISSES       --> 0x10 --> BR_MIS_PRED
     *  5. BUS_CYCLES          --> 0x1D --> BUS_CYCLES
     */
    u32 event_type[6] = {0x08, 0x04, 0x03, 0x0C, 0x10, 0x1D};
    u32 i;

    for (i = 0; i < 6; i++) {
        config_pmn(i, event_type[i]);
    }
}

