#include <string.h>
#include <stdlib.h>
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>
#include <lrk_det_ta.h>

static TEE_Result set_hpc(void);
static TEE_Result check_hpc(void);
static TEE_Result read_cnt(uint32_t param_types, TEE_Param params[4]);
static uint32_t read_pmn(uint32_t counter);
TEE_Result open_sec_file(void *f_name, uint32_t f_size);
TEE_Result open_sec_file_spe(void *f_name, uint32_t f_size);
TEE_Result create_sec_file(void *f_name, uint32_t f_size);
TEE_Result write_sec_file(void *f_name, uint32_t f_size, void *buf, uint32_t len);
TEE_Result read_sec_file(void *f_name, uint32_t f_size, void *buf, uint32_t len);
TEE_Result delete_sec_file(void *f_name, uint32_t f_size);

uint32_t sc_cnt[5][6];
uint32_t sc_sum[5][6];
uint32_t sc_buf[5][6];
TEE_ObjectHandle g_FilesObj;
char *sc_name = "sc_cnt.txt";
uint32_t sc_size = sizeof(sc_name);

TEE_Result TA_CreateEntryPoint(void)
{
	return TEE_SUCCESS;
}

void TA_DestroyEntryPoint(void)
{

}

TEE_Result TA_OpenSessionEntryPoint(uint32_t param_types, TEE_Param __maybe_unused params[4],
		void __maybe_unused **sess_ctx)
{
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	(void)&params;
	(void)&sess_ctx;

	return TEE_SUCCESS;
}

void TA_CloseSessionEntryPoint(void __maybe_unused *sess_ctx)
{
	(void)&sess_ctx; 
}

TEE_Result TA_InvokeCommandEntryPoint(void __maybe_unused *sess_ctx, uint32_t cmd_id,
            uint32_t param_types, TEE_Param params[4])
{
    (void)&sess_ctx; 

    switch (cmd_id) {
        case TA_LKR_DET_CMD_SET_HPC:
            return set_hpc();
        case TA_LKR_DET_CMD_CHECK_HPC:
            return check_hpc();
        case TA_LKR_DET_CMD_READ_CNT:
            return read_cnt(param_types, params);
        default:
            return TEE_ERROR_BAD_PARAMETERS;
    }
}

// system_call:
// 1) sys_getdents64 (61)  
// 2) sys_read       (63)  
// 3) sys_write      (64) 
// 4) sys_openat     (56)
// 5) sys_close      (57)  
uint32_t scno[5] = {61, 63, 64, 56, 57};
char *sys[5] = {"getdents64", "read", "write", "openat", "close"};
char *hpc[6] = {"IN", "CR", "CM", "BI", "BM", "BC"};

static TEE_Result set_hpc(void)
{
    uint32_t i, j;

    IMSG("\e[1m- LKM ROOTKIT DETECTOR SET -\e[0m\n");
    IMSG("\n");
    IMSG("\e[1msys_name    scno  hpc  num\e[0m\n");
    IMSG("---------------------------\n");
    for (i=0; i<5; i++) {
        for (j=0; j<6; j++) {
            sc_cnt[i][j] = sc_sum[i][j] / LOOP_NUM;
            IMSG("%-10s  %-4d  %-3s  %-5d \n", sys[i], scno[i], hpc[j], sc_cnt[i][j]);
        }
        IMSG("\n");
    }
    IMSG("\e[1m----------------------------\e[0m\n");

    delete_sec_file(sc_name, sc_size);
    create_sec_file(sc_name, sc_size);
    write_sec_file(sc_name, sc_size, sc_cnt, sizeof(sc_cnt));

	return TEE_SUCCESS;
}

static TEE_Result check_hpc(void)
{
    char *status[2] = {"\e[1;32mNormal\e[0m", "\e[1;31mAbnormal\e[0m"};
    char *sta;
    uint32_t i, j, pre=0, cur=0;
    int dif=0, div=0, tp=5, tn=-10;

    IMSG("\e[1m------------- LKM ROOTKIT DETECTOR CHECK -------------\e[0m\n");
    IMSG("\n");
    IMSG("\e[1msys_name    scno  hpc  pre    cur    dif   div  status\e[0m\n");
    IMSG("-------------------------------------------------------\n");
    read_sec_file(sc_name, sc_size, sc_buf, sizeof(sc_buf));
    for (i=0; i<5; i++) {
        for (j=0; j<6; j++) {
            sc_cnt[i][j] = sc_sum[i][j] / LOOP_NUM;
            pre = sc_buf[i][j];
            cur = sc_cnt[i][j];
            dif = (int)sc_cnt[i][j]-(int)sc_buf[i][j];
            div = dif*(int)100/(int)pre;
            if (j==2) {
                tp = 15;
                tn = -30;
            } else if (j==4) {
                tp = 6;
                tn = -15;
            } 
            else {
                tp = 4;
                tn = -10;
            }
            if (div>=tp || div<=tn) {
                sta = status[1];
            } else {
                sta = status[0];
            }
            IMSG("%-10s  %-4d  %-3s  %-5d  %-5d  %-4d  %-3d  %-8s\n", 
                sys[i], scno[i], hpc[j], pre, cur, dif, div, sta);
        }
        IMSG("\n");
    }
    IMSG("\e[1m-------------------------------------------------------\e[0m\n");

	return TEE_SUCCESS;
}

static TEE_Result read_cnt(uint32_t param_types, TEE_Param params[4])
{
	uint32_t i, j;
	uint32_t scno = 0;
	uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_VALUE_INOUT, TEE_PARAM_TYPE_NONE,
						   TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);
	
	if (param_types != exp_param_types)
		return TEE_ERROR_BAD_PARAMETERS;

	scno = params[0].value.a;
    switch(scno) {
        case 61: 
            for (i=0; i<6; i++) {
                sc_cnt[0][i] = read_pmn(i);
                sc_sum[0][i] = sc_sum[0][i] + sc_cnt[0][i];
            }
            break;
        case 63: 
            for (i=0; i<6; i++) {
                sc_cnt[1][i] = read_pmn(i);
                sc_sum[1][i] = sc_sum[1][i] + sc_cnt[1][i];
            }
            break;
        case 64: 
            for (i=0; i<6; i++) {
                sc_cnt[2][i] = read_pmn(i);
                sc_sum[2][i] = sc_sum[2][i] + sc_cnt[2][i];
            }
            break;
        case 56: 
            for (i=0; i<6; i++) {
                sc_cnt[3][i] = read_pmn(i);
                sc_sum[3][i] = sc_sum[3][i] + sc_cnt[3][i];
            }
            break;
        case 57: 
            for (i=0; i<6; i++) {
                sc_cnt[4][i] = read_pmn(i);
                sc_sum[4][i] = sc_sum[4][i] + sc_cnt[4][i];
            }
            break;
        default:
            break;
    }
    // IMSG("read_cnt\n");
    return TEE_SUCCESS;
}

static uint32_t read_pmn(uint32_t counter)
{
    uint32_t val = 0;

    switch(counter) {
        case 0:
            asm volatile("mrs %0, PMEVCNTR0_EL0" : "=r"(val));
            break;
        case 1:
            asm volatile("mrs %0, PMEVCNTR1_EL0" : "=r"(val));
            break;
        case 2:
            asm volatile("mrs %0, PMEVCNTR2_EL0" : "=r"(val));
            break;
        case 3:
            asm volatile("mrs %0, PMEVCNTR3_EL0" : "=r"(val));
            break;
        case 4:
            asm volatile("mrs %0, PMEVCNTR4_EL0" : "=r"(val));
            break;
        case 5:
            asm volatile("mrs %0, PMEVCNTR5_EL0" : "=r"(val));
            break;  
        default:
            break;
    }
    return val;
}

TEE_Result open_sec_file(void *f_name, uint32_t f_size)
{
    TEE_Result l_ret = TEE_EXEC_FAIL; 
    uint32_t l_AccFlg = TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_ACCESS_READ;

    l_ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_REE, f_name, 
                                f_size, l_AccFlg, (&g_FilesObj));
    if (TEE_SUCCESS != l_ret) {        
        return TEE_EXEC_FAIL;
    } else {
        return TEE_SUCCESS;
    }
}

TEE_Result open_sec_file_spe(void *f_name, uint32_t f_size)
{
    TEE_Result l_ret = TEE_EXEC_FAIL; 
    uint32_t l_AccFlg = TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_ACCESS_WRITE_META;

    l_ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_REE, f_name, 
                                   f_size, l_AccFlg, (&g_FilesObj));
    if (TEE_SUCCESS != l_ret) {        
        return TEE_EXEC_FAIL;
    } else {
        return TEE_SUCCESS;
    }
}

TEE_Result create_sec_file(void *f_name, uint32_t f_size)
{
    TEE_Result l_ret = TEE_EXEC_FAIL;
    
    // IMSG("[CREATE] start to create file: %s\n", f_name);
    l_ret = TEE_CreatePersistentObject(TEE_STORAGE_PRIVATE_REE, f_name,
                       f_size, TEE_DATA_FLAG_ACCESS_WRITE_META | TEE_DATA_FLAG_ACCESS_WRITE, 
                       TEE_HANDLE_NULL , NULL, 0, (&g_FilesObj));

    if (TEE_SUCCESS != l_ret) {
        // IMSG("[CREATE] create file failed");
        return TEE_EXEC_FAIL;
    } else {
        TEE_CloseObject(g_FilesObj);
        return TEE_SUCCESS;
    }
}

TEE_Result write_sec_file(void *f_name, uint32_t f_size, void *buf, uint32_t len)
{
    TEE_Result l_ret = TEE_SUCCESS;

    l_ret = open_sec_file(f_name, f_size);
    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[WRITE] open file fail\n");
        return TEE_EXEC_FAIL;
    }

    // IMSG("[WRITE] start to write file: %s\n", f_name);
    l_ret = TEE_WriteObjectData(g_FilesObj, buf, len);

    TEE_CloseObject(g_FilesObj);
    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[WRITE] wtire file failed\n");
        return TEE_EXEC_FAIL;
    } else {
        return TEE_SUCCESS;
    }
}

TEE_Result read_sec_file(void *f_name, uint32_t f_size, void *buf, uint32_t len)
{
    TEE_Result l_ret = TEE_SUCCESS;
    uint32_t l_count = 0U;

    l_ret = open_sec_file(f_name, f_size);
    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[READ] open file fail\n");
        return TEE_EXEC_FAIL;
    }

    // IMSG("[READ] start to read file: %s\n", f_name);
    l_ret = TEE_ReadObjectData(g_FilesObj, buf, len, &l_count);

    TEE_CloseObject(g_FilesObj);
    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[READ] read file failed\n");
        return TEE_EXEC_FAIL;
    } else {
        return TEE_SUCCESS;
    }
}

TEE_Result delete_sec_file(void *f_name, uint32_t f_size)
{
    TEE_Result l_ret = TEE_SUCCESS;

    l_ret = open_sec_file_spe(f_name, f_size);
    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[DELETE] open file failed\n");
        return TEE_EXEC_FAIL;
    }

    // IMSG("[DELETE] start to delete file: %s\n", f_name);
    TEE_CloseAndDeletePersistentObject(g_FilesObj);

    if (TEE_SUCCESS != l_ret) {        
        // IMSG("[DELETE] delete file failed\n");
        return TEE_EXEC_FAIL;
    } else {     
        return TEE_SUCCESS;
    }
}

