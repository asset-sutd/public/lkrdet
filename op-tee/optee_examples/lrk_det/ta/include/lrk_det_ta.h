#ifndef TA_LKR_DET_H
#define TA_LKR_DET_H


/*
 * This UUID is generated with uuidgen
 * the ITU-T UUID generator at http://www.itu.int/ITU-T/asn1/uuid.html
 */
#define TA_LKR_DET_UUID \
	{0xd7220062,0x4466,0x4b4d,\
	{0x80, 0x2b, 0x4f, 0x59, 0xe2, 0x19, 0xe4, 0x9e}}

/* The function IDs implemented in this TA */
#define TA_LKR_DET_CMD_SET_HPC		    0
#define TA_LKR_DET_CMD_CHECK_HPC        1
#define TA_LKR_DET_CMD_READ_CNT		    2

#define TEE_EXEC_FAIL    0x0000FFFF
#define LOOP_NUM         5000

#endif 



