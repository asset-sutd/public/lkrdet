# LKRDet

*A framework based on a Trusted Execution Environment (TEE)*
*to detect kernel rootkits in a Rich Execution Environment (REE, e.g., Linux)*
*running on IoT devices.*


### Hardware and software environment
|        Hardware        |        CPU        |         Arch        |            OS              |
|:----------------------:|:-----------------:|:-------------------:|:--------------------------:|
| Raspberry Pi 3 Model B | Broadcom BCM2837  | Cortex-A53(ARMv8)   | Linux 4.6.3/OP-TEE 3.2.0   |


### Files in the Repo
- `op-tee/` : This folder contains lkr_det for detecting rootkits.
- `rootkit/` : This folder contains four kinds of rootkits for testing.
- `test_syscall/` : This is the test app for generating system calls. 

### Build and Run

#### Build

##### Build OP-TEE:   

Copy the files in `op-tee/` to corresponding OP-TEE folders and build them with OP-TEE.    
Refer to the OP-TEE official github https://github.com/OP-TEE/optee_os for building and installation. 

##### Build Rootkits:   

Compile with reference to the `Makefile` in the relevant folders.    
Example:  

```
# cd rootkit/adore-ng/
# make
```

Copy the kernel module files (e.g., `adore-ng.ko`) to the Linux system home folder `/home/root/lkr_det`. 

##### Build Test App: 

Compile with reference to the `Makefile` in the relevant folders.    
Example:  

```
# cd test_syscall/
# make
```
Copy the test app file (i.e., `test_syscall`) to the Linux system folder `/usr/bin`. 

#### Run

##### Offline Profiling

Run `LKRDet` in clean kernel mode to do the offline profiling.    
Example: 

```
# lrk_det set                                                                                                                                                                                
D/TC:0 tee_ta_init_pseudo_ta_session:274 Lookup pseudo TA d7220062-4466-4b4d-802b-4f59e219e49e
D/TC:0 load_elf:842 Lookup user TA ELF d7220062-4466-4b4d-802b-4f59e219e49e (Secure Storage TA)
D/TC:0 load_elf:842 Lookup user TA ELF d7220062-4466-4b4d-802b-4f59e219e49e (REE)
D/TC:0 load_elf_from_store:810 ELF load address 0x40006000
D/TC:0 tee_ta_init_user_ta_session:1019 Processing relocations in d7220062-4466-4b4d-802b-4f59e219e49e
I/TA:  - LKM ROOTKIT DETECTOR SET -
I/TA:  
I/TA:  sys_name    scno  hpc  cnt 
I/TA:  -----------------------------------
I/TA:  getdents64  61    IN   13220
I/TA:  getdents64  61    CR   5184 
I/TA:  getdents64  61    CM   87   
I/TA:  getdents64  61    BI   1452 
I/TA:  getdents64  61    BM   241  
I/TA:  getdents64  61    BC   10918
I/TA:  
I/TA:  read        63    IN   6762 
I/TA:  read        63    CR   3149 
I/TA:  read        63    CM   68   
I/TA:  read        63    BI   866  
I/TA:  read        63    BM   112  
I/TA:  read        63    BC   6678 
I/TA:  
I/TA:  write       64    IN   8779 
I/TA:  write       64    CR   4004 
I/TA:  write       64    CM   132  
I/TA:  write       64    BI   1083 
I/TA:  write       64    BM   183  
I/TA:  write       64    BC   9627 
I/TA:  
I/TA:  openat      56    IN   9707 
I/TA:  openat      56    CR   4445 
I/TA:  openat      56    CM   109  
I/TA:  openat      56    BI   1134 
I/TA:  openat      56    BM   191  
I/TA:  openat      56    BC   9937 
I/TA:  
I/TA:  close       57    IN   6941 
I/TA:  close       57    CR   3240 
I/TA:  close       57    CM   71   
I/TA:  close       57    BI   898  
I/TA:  close       57    BM   104  
I/TA:  close       57    BC   6902 
I/TA:  
I/TA:  ------------------------------------
D/TC:0 tee_ta_close_session:380 tee_ta_close_session(0x8485700)
D/TC:0 tee_ta_close_session:399 Destroy session
D/TC:0 tee_ta_close_session:425 Destroy TA ctx
```

##### Online Monitoring

Insert a kernel rootkit.    
Example:    

```
# insmod adore-ng.ko
```

Run `LKRDet` in rootkit mode to do online monitoring.    
Example: 

```
# lrk_det check                                                                                                                                                                              
D/TC:0 tee_ta_init_pseudo_ta_session:274 Lookup pseudo TA d7220062-4466-4b4d-802b-4f59e219e49e
D/TC:0 load_elf:842 Lookup user TA ELF d7220062-4466-4b4d-802b-4f59e219e49e (Secure Storage TA)
D/TC:0 load_elf:842 Lookup user TA ELF d7220062-4466-4b4d-802b-4f59e219e49e (REE)
D/TC:0 load_elf_from_store:810 ELF load address 0x40006000
D/TC:0 tee_ta_init_user_ta_session:1019 Processing relocations in d7220062-4466-4b4d-802b-4f59e219e49e
I/TA:  ------------- LKM ROOTKIT DETECTOR CHECK -------------
I/TA:  
I/TA:  sys_name    scno  hpc  pre    cur    dev  status
I/TA:  ---------------------------------------------------------------
I/TA:  getdents64  61    IN   13220  14748  11   Abnormal
I/TA:  getdents64  61    CR   5184   5783   11   Abnormal
I/TA:  getdents64  61    CM   87     109    25   Abnormal
I/TA:  getdents64  61    BI   1452   1626   11   Abnormal
I/TA:  getdents64  61    BM   241    283    17   Abnormal
I/TA:  getdents64  61    BC   10918  12421  13   Abnormal
I/TA:  
I/TA:  read        63    IN   6762   6789   0    Normal
I/TA:  read        63    CR   3149   3160   0    Normal
I/TA:  read        63    CM   68     70     2    Normal
I/TA:  read        63    BI   866    870    0    Normal
I/TA:  read        63    BM   112    114    1    Normal
I/TA:  read        63    BC   6678   6717   0    Normal
I/TA:  
I/TA:  write       64    IN   8779   8850   0    Normal
I/TA:  write       64    CR   4004   4035   0    Normal
I/TA:  write       64    CM   132    139    5    Normal
I/TA:  write       64    BI   1083   1092   0    Normal
I/TA:  write       64    BM   183    188    2    Normal
I/TA:  write       64    BC   9627   9736   1    Normal
I/TA:  
I/TA:  openat      56    IN   9707   9718   0    Normal
I/TA:  openat      56    CR   4445   4447   0    Normal
I/TA:  openat      56    CM   109    111    1    Normal
I/TA:  openat      56    BI   1134   1134   0    Normal
I/TA:  openat      56    BM   191    195    2    Normal
I/TA:  openat      56    BC   9937   9948   0    Normal
I/TA:  
I/TA:  close       57    IN   6941   6958   0    Normal
I/TA:  close       57    CR   3240   3248   0    Normal
I/TA:  close       57    CM   71     72     1    Normal
I/TA:  close       57    BI   898    900    0    Normal
I/TA:  close       57    BM   104    105    0    Normal
I/TA:  close       57    BC   6902   6924   0    Normal
I/TA:  
I/TA:  ---------------------------------------------------------------
D/TC:0 tee_ta_close_session:380 tee_ta_close_session(0x8485700)
D/TC:0 tee_ta_close_session:399 Destroy session
D/TC:0 tee_ta_close_session:425 Destroy TA ctx
```


